-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Mei 2018 pada 06.18
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apotek`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `username`, `password`, `foto`, `level`) VALUES
(10, 'asdfgh', 'asdfg', '1234', '', ''),
(11, 'ftftf', 'aaaaaa', '12345', '', ''),
(13, 'farid', 'farid', 'a1d12da42d4302e53d510954344ad164', '', ''),
(15, 'farid2', 'farid2', '202cb962ac59075b964b07152d234b70', '', ''),
(16, 'budi', 'budi', '202cb962ac59075b964b07152d234b70', '', ''),
(17, 'aaaaa', 'aaaaa', '202cb962ac59075b964b07152d234b70', '', ''),
(18, 'qwqerrt', 'qqq', '827ccb0eea8a706c4c34a16891f84e7b', '', ''),
(19, 'lala', 'lala', '2e3817293fc275dbee74bd71ce6eb056', '', ''),
(20, 'booo', 'lalal', 'ae3e83e2fab3a7d8683d8eefabd1e74d', '', ''),
(21, 'Farid Priya Nugraha', 'farid', '6b94b5250e58b9087acaddc6ec81965e', '', ''),
(22, 'kiki', 'kiki', '0d61130a6dd5eea85c2c5facfe1c15a7', '', ''),
(23, 'aliya', 'aliya', '202cb962ac59075b964b07152d234b70', '', ''),
(24, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', 'admin'),
(25, 'Nugraha', 'kasir', 'c7911af3adbd12a035b289556d96470a', '', 'kasir'),
(26, 'budi budiman', 'budi', '00dfc53ee86af02e742515cdcf075ed3', '', 'kasir');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasir`
--

CREATE TABLE `kasir` (
  `id_kasir` int(11) NOT NULL,
  `nama_kasir` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kasir`
--

INSERT INTO `kasir` (`id_kasir`, `nama_kasir`, `username`, `password`) VALUES
(1, 'farid nugraha', 'farid', 'farid');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` varchar(50) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
('OB', 'Obat Bebas'),
('OBT', 'Obat Bebas Terbatas Sekali'),
('OK', 'Obat Keras');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nota`
--

CREATE TABLE `nota` (
  `id_nota` int(11) NOT NULL,
  `tgl_beli` date NOT NULL,
  `grandtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nota`
--

INSERT INTO `nota` (`id_nota`, `tgl_beli`, `grandtotal`) VALUES
(1, '2018-05-30', 6000),
(2, '2018-05-30', 6000),
(3, '2018-05-30', 6000),
(4, '2018-05-30', 8000),
(5, '2018-05-30', 8000),
(6, '2018-05-30', 8000),
(7, '2018-05-30', 8000),
(8, '2018-05-30', 8000),
(9, '2018-05-30', 8000),
(10, '2018-05-30', 8000),
(11, '2018-05-30', 8000),
(12, '2018-05-30', 8000),
(13, '2018-05-30', 8000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat`
--

CREATE TABLE `obat` (
  `id_obat` int(11) NOT NULL,
  `id_kategori` varchar(50) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `stok` int(50) NOT NULL,
  `gambar_obat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `obat`
--

INSERT INTO `obat` (`id_obat`, `id_kategori`, `nama_obat`, `harga`, `deskripsi`, `stok`, `gambar_obat`) VALUES
(2, 'OK', 'batuk ', '2000', 'obat bagus', 12, ''),
(9, 'OBT', 'pusing', '3000', 'Obat khusus anak dewas', 12, ''),
(12, 'OB', 'Panas', '3000', 'obat bagus sekali', 10, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembeli`
--

CREATE TABLE `pembeli` (
  `id_pembeli` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usia` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_nota` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_nota`, `id_obat`, `jumlah`) VALUES
(1, 1, 12, 2),
(2, 2, 12, 2),
(3, 3, 12, 2),
(4, 4, 12, 2),
(5, 4, 2, 1),
(6, 5, 12, 2),
(7, 5, 2, 1),
(8, 6, 12, 2),
(9, 6, 2, 1),
(10, 7, 12, 2),
(11, 7, 2, 1),
(12, 8, 12, 2),
(13, 8, 2, 1),
(14, 9, 12, 2),
(15, 9, 2, 1),
(16, 10, 12, 2),
(17, 10, 2, 1),
(18, 11, 12, 2),
(19, 11, 2, 1),
(20, 12, 12, 2),
(21, 12, 2, 1),
(22, 13, 12, 2),
(23, 13, 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`id_kasir`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_nota` (`id_nota`),
  ADD KEY `id_obat` (`id_obat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `kasir`
--
ALTER TABLE `kasir`
  MODIFY `id_kasir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nota`
--
ALTER TABLE `nota`
  MODIFY `id_nota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id_pembeli` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_nota`) REFERENCES `nota` (`id_nota`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
