<h2>Update Obat</h2>
<div class="col-md-12">
	<?php foreach ($obat as $obt) {?>

	<form action="<?=base_url('index.php/obat/edit_obat')?>" method="post">
			<input type="hidden" name="id_obat" value="<?=$obt->id_obat?>">

  			<div class="form-group">
  				<input type="text" class="form-control" placeholder="Nama Obat" name="nama_obat" value="<?=$obt->nama_obat?>">
  			</div>

            <div class="form-group">
              <select name="kategori" class="form-control">
                <option><?=$obt->id_kategori?></option>
              <?php foreach ($tampil_kategori as $kat): ?>
                <option value="<?=$kat->id_kategori?>">
                  <?=$kat->nama_kategori ?></option>
              <?php endforeach ?>
              </select>
            </div>

            <div class="form-group">
              <input type="number" class="form-control" placeholder="Harga" name="harga" value="<?=$obt->harga?>">
            </div>

            <div class="form-group">
              <input type="text" class="form-control" placeholder="Deskrisi" name="deskripsi" value="<?=$obt->deskripsi?>">
            </div>

            <div class="form-group">
              <input type="number" class="form-control" placeholder="Stok" name="stok" value="<?=$obt->stok?>">
            </div>

           <!-- <div class="form-group">
              <input type="file" class="form-control" name="gambar">
            </div>	-->
            <input type="submit" name="edit-obat" class="btn btn-success" value="Edit"></input>
		</form>
</div>
		
<?php } ?>
