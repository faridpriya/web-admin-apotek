<h2>Transaksi</h2>

<div class="col-md-6">
	 <table class="table table-striped">
	 	 <tr>
		    <th style="text-align: center;">No</th>
		    <th style="text-align: center;">Nama Obat</th>
		    <th style="text-align: center;">Harga</th>
		    <th style="text-align: center;">Aksi</th>
		 </tr>
		 <?php $no=0;foreach ($obat as $obt):$no++;?>
		 <tr>
		 	<td style="text-align: center;"><?=$no?></td>
		 	<td style="text-align: center;"><?=$obt->nama_obat?></td>
		 	<td style="text-align: center;"><?=$obt->harga?></td>
		 	<td style="text-align: center;"><a class="btn btn-warning" href="<?=base_url('index.php/transaksi/addcart/'.$obt->id_obat)?>">Beli</td>
		 </tr>
		<?php endforeach ?>
	 </table>
</div>

<div class="col-md-6"></div>
<form action="<?=base_url('index.php/transaksi/simpan')?>" method="post">
 <table class="table table-striped">
	 	 <tr>
		    <th style="text-align: center;">No</th>
		    <th style="text-align: center;">Nama Obat</th>
		    <th style="text-align: center;">Qty</th>
		    <th style="text-align: center;">Harga</th>
		    <th style="text-align: center;">Subtotal</th>
		    <th style="text-align: center;">Aksi</th>
		 </tr>
		 <?php $no=0;foreach ($this->cart->contents() as $items):$no++;?>
		 <input type="hidden" name="id_obat[]" value="<?=$items['id']?>">
		 <input type="hidden" name="rowid[]" value="<?=$items['rowid']?>">

		 <tr>
		    <td style="text-align: center;"><?=$no?></td>
		    <td style="text-align: center;"><?=$items['name']?></td>
		    <td style="text-align: center;padding: 4px;"><input type="number" name="qty[]" value="<?=$items['qty']?>" class="form-control"></td>
		    <td style="text-align: center;"><?=number_format($items['price'])?></td>
		    <td style="text-align: center;"><?=number_format($items['subtotal'])?></td>
		    <td style="text-align: center;"><a class="btn btn-danger" href="<?=base_url('index.php/transaksi/hapus_cart/'.$items['rowid'])?>">&times;</a></td>
		 </tr>
		 <?php endforeach ?>

		 <input type="hidden" name="grandtotal" value="<?=$this->cart->total()?>">
		 <tr style="border-bottom:5px black solid">
		 	<th colspan="5">Grandtotal</th>
		 	<th style="text-align: center;"><?=number_format($this->cart->total())?></th>
		 </tr>
</table>

<input type="submit" class="btn btn-success" name="update" value="update QTY">
<input type="submit" class="btn btn-danger" name="bayar" value="Bayar" onclick="return confirm('apakah anda yakin?')">
</form>