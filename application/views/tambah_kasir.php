<h2>Tambah Kasir</h2>
<div class="col-md-6 col-md-offset-3">
	<form class="form-auth-small" action="<?=base_url('index.php/admin/add_kasir')?>" method="post">
		<input type="hidden" name="level" value="kasir">
		<div class="form-group">
			<label class="control-label sr-only">Nama</label>
			<input type="text" class="form-control" placeholder="Name" name="nama_admin">
		</div>
		<div class="form-group">
			<label class="control-label sr-only">Username</label>
			<input type="text" class="form-control" placeholder="Username" name="username">
		</div>
		<div class="form-group">
			<label class="control-label sr-only">Password</label>
			<input type="password" class="form-control" placeholder="Password" name="password">
		</div>
			<input type="hidden" name="level" value="kasir">
		<input type="submit" name="tambah-kasir" class="btn btn-primary btn-lg btn-block" value="Register"></input>
	</form>

	<div class="col-md-5 col-md-offset-3">
	<?php if ($this->session->userdata('pesan') != null):?>
	  <div class="alert alert-danger"> <?= $this->session->flashdata('pesan');?> </div>
	<?php endif ?>
</div>
</div>