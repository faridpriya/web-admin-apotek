<h2 style="padding: 10px;">Daftar Obat</h2>
<a href="#tambah" data-toggle="modal" class="btn btn-warning" style="float: right;margin:15px;"><span class="glyphicon glyphicon-plus" style="padding-right: 2px"></span>Tambah</a>
<div class="col-md-12">
  <table class="table table-striped">
  <tr>
    <th style="text-align: center;">No</th>
    <th style="text-align: center;">Nama Obat</th>
    <th style="text-align: center;">Id Kategori</th>
    <th style="text-align: center;">Deskripsi</th>
    <th style="text-align: center;">Stok</th>
    <th style="text-align: center;">Harga</th>
    <th style="text-align: center;">Aksi</th>        
  </tr>

  <?php 
  $no=0;foreach ($tampil_obat as $kat):$no++;?>

  <tr style="text-align: center;">
    <td><?=$no?></td>
    <td><?=$kat->nama_obat?></td>
    <td><?=$kat->id_kategori?></td>
    <td><?=$kat->deskripsi?></td>
    <td><?=$kat->stok?></td>
    <td><?=$kat->harga?></td>
    <td><a href="<?=base_url('index.php/obat/update_tampil/'.$kat->id_obat)?>" onclick="edit('<?=$kat->id_obat?>')" data-toggle="modal" class="btn btn-success">Ubah</a> <a href="<?=base_url('index.php/obat/hapus_obat/'.$kat->id_obat)?>" onclick="return confirm('anda yakin untuk menghapus?')" class="btn btn-danger">Hapus</a></td>
  </tr>
<?php endforeach?>
</table>

</div>
<div class="col-md-5 col-md-offset-3">
<?php if ($this->session->userdata('pesan') != null):?>
  <div class="alert alert-danger"> <?= $this->session->flashdata('pesan');?> </div>
<?php endif ?>
</div>

<!--Poopup tambah-->
<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Tambah Kategori</h4>
      </div>
      <div class="modal-body">
        	<form action="<?=base_url('index.php/obat/tambah_obt')?>" method="post">
  					<div class="form-group">
  						<input type="text" class="form-control" placeholder="Nama Obat" name="nama_obat">
  					</div>

            <div class="form-group">
              <select name="kategori" class="form-control">
                <option></option>
              <?php foreach ($tampil_kategori as $kat): ?>
                <option value="<?=$kat->id_kategori?>">
                  <?=$kat->nama_kategori ?></option>
              <?php endforeach ?>
              </select>
            </div>

            <div class="form-group">
              <input type="number" class="form-control" placeholder="Harga" name="harga">
            </div>

            <div class="form-group">
              <input type="text" class="form-control" placeholder="Deskrisi" name="deskripsi">
            </div>

            <div class="form-group">
              <input type="number" class="form-control" placeholder="Stok" name="stok">
            </div>

            <div class="form-group">
              <input type="file" class="form-control" name="gambar">
            </div>	

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<input type="submit" name="tambah-obat" class="btn btn-primary" value="Tambah"></input>
		</form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
