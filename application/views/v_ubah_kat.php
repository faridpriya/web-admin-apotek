<h2>update kategori</h2>
<div class="col-md-5 col-md-offset-3">
<?php foreach ($kategori as $kat) {?>

<form action="<?=base_url('index.php/kategori/edit')?>" method="post">

	<div class="form-group">
		<input type="text" class="form-control" name="id_kategori" id="id_kategori" value="<?php echo $kat->id_kategori ?>">
	</div>
	<div class="form-group">
		<input type="text" class="form-control" name="nama_kategori" id="nama_kategori" value="<?php echo $kat->nama_kategori ?>">
	</div>

	<div class="form-group">
		<input type="submit" class="btn btn-success" value="Ubah" name="ubah">
	</div>

	<?php if ($this->session->flashdata('pesan') != null):?>
		<div class="alert alert-danger"><?= $this->session->flashdata('pesan');?></div>
	<?php endif ?>
</form>
<?php } ?>
</div>
