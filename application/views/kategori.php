<h2 style="padding: 10px;">Kategori Obat</h2>
<a href="#tambah" data-toggle="modal" class="btn btn-warning" style="float: right;margin:15px;"><span class="glyphicon glyphicon-plus" style="padding-right: 2px"></span>Tambah</a>
<div class="col-md-12">
<table class="table table-striped">
	<tr>
		<th style="text-align: center;">No</th>
		<th style="text-align: center;">Id</th>
		<th style="text-align: center;">Nama kategori</th>
		<th style="text-align: center;">Aksi</th>
	</tr>

	<?php 
	$no=0;foreach ($tampil_kategori as $kat):$no++;?>

	<tr style="text-align: center;">
		<td><?=$no?></td>
		<td><?=$kat->id_kategori?></td>
		<td><?=$kat->nama_kategori?></td>
		<td><a href="<?=base_url('index.php/kategori/update/'.$kat->id_kategori)?>" onclick="edit('<?=$kat->id_kategori?>')" data-toggle="modal" class="btn btn-success">Ubah</a> <a href="<?=base_url('index.php/kategori/hapus/'.$kat->id_kategori)?>" onclick="return confirm('anda yakin untuk menghapus?')" class="btn btn-danger">Hapus</a></td>
	</tr>
<?php endforeach?>
</table>


<!--Poopup tambah-->
<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Tambah Kategori</h4>
      </div>
      <div class="modal-body">
        	<form action="<?=base_url('index.php/kategori/tambah_kat')?>" method="post">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="ID" name="id_kategori">
						</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Nama Kategori" name="nama_kategori">
					</div>	

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<input type="submit" name="tambah-kategori" class="btn btn-primary" value="Tambah"></input>
		</form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--Poopup update-->
<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Tambah Kategori</h4>
      </div>
      <div class="modal-body">
        	<form action="<?=base_url('index.php/kategori/ubah_kat')?>" method="post">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="ID" name="id_kategori" id="id_kategori">
						</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Nama Kategori" name="nama_kategori" id="nama_kategori">
					</div>	

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<input type="submit" name="ubah-kategori" class="btn btn-success" value="Simpan"></input>
		</form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
