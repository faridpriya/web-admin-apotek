<div class="col-md-5 col-md-offset-3" style="background: white;margin-top: 5%">
<h2 align="center">Nota Pembelian</h2>
<h5>No Nota :<?=$nota->id_nota?><h5>
<h5>Tanggal Beli :<?=$nota->tgl_beli?><h5>
	<hr>
<table border=1 style="border-collapse: collapse;">
	<tr>
		<th>No</th>
		<th>Nama Obat</th>
		<th>Harga</th>
		<th>Qty</th>
		<th>Subtotal</th>
	</tr>
	<?php $this->load->model('m_transaksi'); $no=0; foreach ($this->m_transaksi->detail_transaksi($nota->id_nota) as $obat): $no++ ?>

		<tr>
			<td><?=$no?></td>
			<td><?=$obat->nama_obat?></td>
			<td><?=number_format($obat->harga)?></td>
			<td><?=$obat->jumlah?></td>
			<td><?=number_format(($obat->harga * $obat->jumlah))?></td>
		</tr>
	<?php endforeach ?>

	<tr>
		<th colspan="4">Grand Total</th>
		<th><?=number_format($nota->grandtotal)?></th>
	</tr>
</table>