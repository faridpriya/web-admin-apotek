<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_obat extends CI_Model {

	public function data_kategori()
	{
		return $this->db->get('kategori')->result();
	}

	public function tampil_obat()
	{
		$tm_obat=$this->db->join('kategori','kategori.id_kategori=obat.id_kategori')->get('obat')->result();
		return $tm_obat;	
	}

	public function detail($id)
	{
		$tm_obat=$this->db
		->join('kategori','kategori.id_kategori=obat.id_kategori')
		->where('id_obat',$id)
		->get('obat')
		->row();
		return $tm_obat;
	}

	public function simpan_obat($nama_file)
	{
		if ($nama_file =="") {
			$object = array(
				'nama_obat'=>$this->input->post('nama_obat'),
				'id_kategori'=>$this->input->post('kategori'),
				'harga'=>$this->input->post('harga'),
				'deskripsi'=>$this->input->post('deskripsi'),
				'stok'=>$this->input->post('stok')
			);
		}else{
			$object = array(
				'nama_obat'=>$this->input->post('nama_obat'),
				'id_kategori'=>$this->input->post('kategori'),
				'harga'=>$this->input->post('harga'),
				'deskripsi'=>$this->input->post('deskripsi'),
				'stok'=>$this->input->post('stok'),
				'gambar_obat'=>$nama_file
			);
		}
		return $this->db->insert('obat',$object);
	}

	public function hapus_obat($id_obat='')
	{
		return $this->db->where('id_obat',$id_obat)->delete('obat');
	}

	public function update_obat($where,$table)
	{
		return $this->db->get_where($table,$where);
	}

	public function edit_obat()
	{
		$id_obat=$this->input->post('id_obat');

		$kategori=$this->input->post('kategori');
		$nama_obat=$this->input->post('nama_obat');
		$harga=$this->input->post('harga');
		$deskripsi=$this->input->post('deskripsi');
		$stok=$this->input->post('stok');

		$object = array(
			'id_kategori'=>$kategori,
			'nama_obat'=>$nama_obat,
			'harga'=>$harga,
			'deskripsi'=>$deskripsi,
			'stok'=>$stok
		);

		$where=array('id_obat'=>$id_obat);
		return $this->db->where($where)->update('obat',$object);
	}

}

/* End of file M_obat.php */
/* Location: ./application/models/M_obat.php */