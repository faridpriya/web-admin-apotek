<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori extends CI_Model {

	public function tampil_kat()
	{
		$tampil_kat=$this->db->get('kategori')->result();
		return $tampil_kat;

		//mengurutkan
		$this->db->from($this->kategori);
		$this->db->order_by("id_kategori", "ASC");
		$query = $this->db->get(); 
		return $query->result();
	}
	//simpan kategori
	public function simpan_kat()
	{
		$id_kategori=$this->input->post('id_kategori');
		$nama_kategori=$this->input->post('nama_kategori');

		$kate=array(
			'id_kategori'=>$id_kategori,
			'nama_kategori'=>$nama_kategori
		);
		return $this->db->insert('kategori',$kate);
	}
	//hapus kategori
	public function hapus_kat($id_kategori='')
	{
		return $this->db->where('id_kategori',$id_kategori)->delete('kategori');
	}

	//tampil update kategori
	public function ambil_kategori($where,$table)
	{
		return $this->db->get_where($table,$where);
	}

	public function edit_kategori()
	{
		$id_kategori=$this->input->post('id_kategori');
		$nama_kategori=$this->input->post('nama_kategori');

		$object = array(
			'id_kategori'=>$id_kategori,
			'nama_kategori'=>$nama_kategori
		);

		$where=array('id_kategori'=>$id_kategori);

		return $this->db->where($where)->update('kategori',$object);
	}
}

/* End of file M_kategori.php */
/* Location: ./application/models/M_kategori.php */