<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaksi extends CI_Model {

	public function simpan_cart_db()
	{
		//$tgl = date('Y-m-d',mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('y')));

		$object = array(
			'tgl_beli'=>date('Y-m-d'),
			'grandtotal'=>$this->input->post('grandtotal')
		);
		$this->db->insert('nota',$object);
		$tm_nota=$this->db->order_by('id_nota','desc')
						->limit(1)
						->get('nota')
						->row();

		for ($i=0; $i <count($this->input->post('rowid')) ; $i++) { 
			$hasil[]=array(
				'id_nota'=>$tm_nota->id_nota,
				'id_obat'=>$this->input->post('id_obat')[$i],
				'jumlah'=>$this->input->post('qty')[$i]
			);
		}
		$proses=$this->db->insert_batch('transaksi',$hasil);
		if ($proses) {
			return $tm_nota->id_nota;
		}else{
			return 0;
		}
	}

	public function detail_nota($id_nota)
	{
		return $this->db->where('id_nota',$id_nota)->get('nota')->row();
	}

	public function detail_transaksi($id_nota)
	{
		return $this->db->where('id_nota',$id_nota)
					->join('obat','obat.id_obat=transaksi.id_obat')
					->join('kategori','kategori.id_kategori=obat.id_kategori')
					->get('transaksi')
					->result();
	}

}

/* End of file M_transaksi.php */
/* Location: ./application/models/M_transaksi.php */