<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

//model register
	public function simpan_data()
	{
		$nama_admin=$this->input->post('nama_admin');
		$username=$this->input->post('username');
		$level=$this->input->post('level');
		$password=$this->input->post('password');

		$data_simpan=array(
			'nama_admin'=>$nama_admin,
			'username'=>$username,
			'level'=>$level,
			'password'=>md5($password)
		);
		return $this->db->insert('admin',$data_simpan);
	}

//tambah kasir
	public function simpan_kasir()
	{
		$nama_kasir=$this->input->post('nama_kasir');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$level=$this->input->post('level');

		$object = array(
			'nama_kasir'=>$nama_kasir,
			'username'=>$username,
			'password'=>$password,
			'level'=>$level
		);

		return $this->db->insert('admin',$object);
	}

//model login
	public function get_login()
	{
		return $this->db
			->where('username',$this->input->post('username'))
			->where('password',md5($this->input->post('password')))
			->get('admin');
	}

	public function get_login_kasir()
	{
		return $this->db
					->where('username',$this->input->post('username'))
					->where('password',$this->input->post('password'))
					->get('kasir');
	}

}
 
/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */