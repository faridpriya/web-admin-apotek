<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function index()
	{
		$this->load->model('m_obat');

		if ($this->session->userdata('login') !=True) {
			redirect('admin/login','refresh');
		}
		$data['tampil_obat']=$this->m_obat->tampil_obat();
		$data['tampil_kategori']=$this->m_obat->data_kategori();
		$data['konten']="obat";
		$this->load->view('template', $data);
	}

	//tambah obat
	public function tambah_obt()
	{
		$this->load->model('m_obat');
		$this->form_validation->set_rules('nama_obat', 'nama_obat', 'trim|required');
		$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
		$this->form_validation->set_rules('stok', 'stok', 'trim|required');

		if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './asset/gambar/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			if($_FILES['gambar']['name']!=""){
				$this->load->library('upload', $config);
			
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', $this->upload->display_errors());
				}
				else{
					if($this->m_obat->simpan_obat($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'sukses menambah');	
					} else {
						$this->session->set_flashdata('pesan', 'gagal menambah');	
					}
					redirect('obat','refresh');		
				}
			} else {
				if($this->m_obat->simpan_obat('')){
					$this->session->set_flashdata('pesan', 'sukses menambah');	
				} else {
					$this->session->set_flashdata('pesan', 'gagal menambah');	
				}
				redirect('obat','refresh');	
			}
			
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('obat','refresh');
		}
	
	}

	//tampil obat
	public function hapus_obat($id_obat='')
	{
		$this->load->model('m_obat');
		if ($this->m_obat->hapus_obat($id_obat)) {
			redirect('obat','refresh');
		}else{
			redirect('obat');
		}
	}

	//update obat
	public function update_tampil($id_obat='')
	{
		$this->load->model('m_obat');
		$where=array('id_obat'=>$id_obat);

		$data['obat']=$this->m_obat->update_obat($where,'obat')->result();
		$data['tampil_kategori']=$this->m_obat->data_kategori();
		$data['konten']="v_ubah_obt";
		$this->load->view('template', $data);
	}

	//edit obat

	public function edit_obat()
	{
		$this->load->model('m_obat');
		if ($this->input->post('edit-obat')) {
			if ($this->m_obat->edit_obat() == True) {
				redirect('obat','refresh');
			}else{
				redirect('obat/update_tampil');
			}
		}
	}

}

/* End of file obat.php */
/* Location: ./application/controllers/obat.php */