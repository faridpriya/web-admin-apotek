<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('login')!=TRUE) {
			redirect('admin/login','refresh');
		}
		$data['konten']="konten";
		$this->load->view('template',$data); 
	}
	public function login()
	{
		if ($this->session->userdata('login')==TRUE) {
			redirect('admin','refresh');
		}else{
			$this->load->view('login');
		}
	}
	public function register()
	{
		$this->load->view('register');
	}

//contoler register
	public function simpan() 
	{

		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('nama_admin', 'nama lengkap', 'trim|required');
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_admin');

				if ($this->m_admin->simpan_data()==TRUE) {
					$this->session->set_flashdata('pesan', 'sukses simpan');
					redirect('admin/login','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'gagal simpan');
					redirect('admin/register','refresh');
				}
			} else{
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('admin/register','refresh');
			}
		}
	}


//controller Login
	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_admin');
				if ($this->m_admin->get_login()->num_rows()>0) {
					$data=$this->m_admin->get_login()->row();
					$array = array(
						'login'=>TRUE,
						'nama_admin'=>$data->nama_admin,
						'username'=>$data->username,
						'password'=>$data->password,
						'foto'=>$data->foto,
						'id_admin'=>$data->id_admin,
						'level'=>$data->level
					);
					$this->session->set_userdata($array);
					redirect('admin','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'username dan password salah');
					redirect('admin/login','refresh');
				}
			} else{
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('admin/login','refresh');
			}
		}
	}

//Logout
	public function logout($value='')
	{
		$this->session->sess_destroy();
		redirect('admin/login','refresh');
	}

//tambah kasir
	public function tambah_kasir()
	{
		$data['konten']="tambah_kasir";
		$this->load->view('template', $data);
	}

//add kasir
	public function add_kasir()
	{
		$this->load->model('m_admin');
		if ($this->input->post('tambah-kasir')) {
			$this->form_validation->set_rules('nama_admin', 'Nama Kasir', 'trim|required');
			$this->form_validation->set_rules('username', 'Nama Kasir', 'trim|required');
			$this->form_validation->set_rules('password', 'Nama Kasir', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				if ($this->m_admin->simpan_data() == TRUE) {
					$this->session->set_flashdata('pesan', 'sukses');
					redirect('admin','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'gagal');
					redirect('admin/tambah_kasir','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('admin/tambah_kasir','refresh');
			}
		}
	}


}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */