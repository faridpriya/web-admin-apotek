<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('login') != True) {
			redirect('admin/login','refresh');
		}

		$this->load->model('m_kategori','kat');
		$data['tampil_kategori']=$this->kat->tampil_kat();
		$data['konten']="kategori";
		$this->load->view('template', $data);
	}
//tambah kategori
	public function tambah_kat()
	{
		$this->load->model('m_kategori','kat');//menjadikam m_kategori menjadi kat

		if ($this->input->post('tambah-kategori')) {
			$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
			$this->form_validation->set_rules('nama_kategori', 'nama kategori', 'trim|required');

			if($this->form_validation->run() == TRUE){
				if ($this->kat->simpan_kat()) {
					redirect('kategori');
				}else{
					redirect('kategori');
				}
			}else{
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('kategori','refresh');
			}
		}
	}
//hapus kategori
	public function hapus($id_kategori='')
	{
		$this->load->model('m_kategori');

		if ($this->m_kategori->hapus_kat($id_kategori)) {
			redirect('kategori','refresh');
		}
	}
//tampil update kategori
	public function update($id_kategori='')
	{
		$this->load->model('m_kategori');
		$where=array('id_kategori'=>$id_kategori);

		$data['kategori']=$this->m_kategori->ambil_kategori($where,'kategori')->result();
		$data['konten']="v_ubah_kat";
		$this->load->view('template', $data);
	}
//ubah kategori
	public function edit()
	{
		$this->load->model('m_kategori');
		if ($this->input->post('ubah')) {
			$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
			$this->form_validation->set_rules('nama_kategori', 'nama kategori', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				if($this->m_kategori->edit_kategori()==True){
					redirect('kategori','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'data harus di isi lengkap');
					redirect('kategori/update','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('kategori/update','refresh');
			}
		}
	}
	
}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */