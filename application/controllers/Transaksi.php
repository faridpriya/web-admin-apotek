<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function index()
	{
		$this->load->model('m_obat');
		$data['obat']=$this->m_obat->tampil_obat();
		$data['konten']="transaksi";
		$this->load->view('template', $data);
	}

	public function addcart($id)
	{
		$this->load->model('m_obat');
		$detail=$this->m_obat->detail($id);

		$data = array(
			'id'      => $detail->id_obat,
			'qty'     => 1,
			'price'   => $detail->harga,
			'name'    => $detail->nama_obat,
			'options' => array('genre'=>$detail->id_kategori)
		);
		
		$this->cart->insert($data);
		redirect('transaksi');
	}

	public function hapus_cart($rowid)
	{
		$this->cart->remove($rowid);
		redirect('transaksi');
	}

	public function simpan()
	{
		if ($this->input->post('update')) {
			for ($i=0; $i < count($this->input->post('rowid')); $i++) { 
				$data = array(
					'rowid'=>$this->input->post('rowid')[$i],
					'qty'=>$this->input->post('qty')[$i]
				);
				$this->cart->update($data);
			}
			redirect('transaksi','refresh');
		}elseif ($this->input->post('bayar')) {
			$this->load->model('m_transaksi');
			$id=$this->m_transaksi->simpan_cart_db();

			if ($id) {
				$data['nota']=$this->m_transaksi->detail_nota($id);
				$data['konten']="cetak_nota";
				$this->load->view('template', $data);
			}else{
				redirect('transaksi','refresh');
			}
		}
	}

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */